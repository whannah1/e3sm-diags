import os
# from acme_diags.parameter.core_parameter import CoreParameter
# from acme_diags.run import runner
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

ref_name = 'E3SM.PGVAL.ne30_ne30.FC5AV1C-L.maint1-4ad402'
test_name = 'E3SM.PGVAL.ne30_ne30.FC5AV1C-L.master-12cf5f'
reference_data_path = '/global/cscratch1/sd/whannah/acme_scratch/cori-knl/'+ref_name+'/clim/'
test_data_path      = '/global/cscratch1/sd/whannah/acme_scratch/cori-knl/'+test_name+'/clim/'

diff_title = 'maint1 - master'

# param = CoreParameter()
# param.reference_data_path = ref_data_path
# param.test_data_path = test_data_path
# param.test_name = '20161118.beta0.FC5COSP.ne30_ne30.edison'
# param.seasons = ["ANN"]   #all seasons ["ANN","DJF", "MAM", "JJA", "SON"] will run,if comment out"


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------

# default sets : ['zonal_mean_xy', 'zonal_mean_2d', 'lat_lon', 'polar', 'cosp_histogram']
sets = ['zonal_mean_xy', 'zonal_mean_2d', 'lat_lon']

# 'mpl' and 'vcs' are for matplotlib or vcs plots respectively.
backend = 'mpl'

# Name of folder where all results will be stored.
results_dir = test_name

# Optional settings below:

# diff_title = 'np4 - pg2'

multiprocessing = True
num_workers =  32    # for haswell
# num_workers =  64    # for KNL