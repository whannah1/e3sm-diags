import os
import glob

cases = []
cases.append('E3SM.RGMA.ne30pg2_r05_oECv3.F-MMF1.CRMNX_64.CRMDX_2000.RADNX_4.00')
cases.append('E3SM.RGMA.ne30pg2_r05_oECv3.FC5AV1C-L.00')
cases.append('E3SM.RGMA.ne120pg2_r05_oECv3.FC5AV1C-H01A.00')

# search_string = '_climo_cmip6_180x360_aave'
# search_string = '_cmip6_180x360_aave'
search_string = '_climo'

for case in cases:
  clim_path = f'/global/cscratch1/sd/whannah/e3sm_scratch/cori-knl/{case}/clim'
  
  print(f'climo path: {clim_path} \n')

  # files = glob.glob(f'{clim_path}/*_climo_*')
  files = glob.glob(f'{clim_path}/*_climo*')

  files = sorted(files)

  for file in files:
    if search_string in file:
      new_file = file.replace(search_string,'')

      tmp_file_old =     file.replace(clim_path+'/','')
      tmp_file_new = new_file.replace(clim_path+'/','')
      print(f'  renaming {tmp_file_old} > {tmp_file_new}')
      os.rename(file,new_file)
  # exit()