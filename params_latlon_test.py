#-------------------------------------------------------------------------------
# reference path 
#-------------------------------------------------------------------------------
# reference_data_path = '/global/project/projectdirs/acme/acme_diags/obs_for_e3sm_diags/climatology/'


ref_name = 'E3SM_TEST_ne30_FC5AV1C-L'
reference_data_path = '/global/cscratch1/sd/whannah/acme_scratch/cori-knl/'+ref_name+'/clim/'

#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# test_name = '20161118.beta0.FC5COSP.ne30_ne30.edison'
# test_data_path = '/global/project/projectdirs/acme/acme_diags/test_model_data_for_acme_diags/climatology/'

test_name = 'E3SM_TEST_ne30pg2_FC5AV1C-L_00'
test_data_path = '/global/cscratch1/sd/whannah/acme_scratch/cori-knl/'+test_name+'/clim/'


#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
sets = ["lat_lon"]
seasons = ["ANN"]

# 'mpl' and 'vcs' are for matplotlib or vcs plots respectively.
backend = 'mpl'

# Name of folder where all results will be stored.
results_dir = 'lat_lon_demo'

