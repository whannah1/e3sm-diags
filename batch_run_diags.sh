#!/bin/bash -l
#SBATCH --partition=regular
#SBATCH --time=01:00:00
#SBATCH --account=m3312
#SBATCH --nodes=1
#SBATCH -C haswell
###SBATCH --job-name=diags
###SBATCH --output=diags.o%j
#SBATCH --mail-type=END,FAIL
#SBATCH --mail-user=hannah6@llnl.gov

### Run this script with: 
# sbatch batch_run_diags.sh
### or this:
# CASE=E3SM.PGVAL.ne30pg2_r05_oECv3.F2010SC5-CMIP6.master-cbe53b; sbatch --job-name=e3sm_diags_${CASE} --output=$HOME/e3sm_diags/logs/slurm-%x-%j.out --export=CASE=$CASE $HOME/e3sm_diags/batch_run_diags.sh
# CASE=E3SM.PGVAL.conusx4v1pg2_r05_oECv3.F2010SC5-CMIP6.master-cbe53b; sbatch --job-name=e3sm_diags_${CASE} --output=$HOME/e3sm_diags/logs/slurm-%x-%j.out --export=CASE=$CASE $HOME/e3sm_diags/batch_run_diags.sh

source /global/cfs/cdirs/e3sm/software/anaconda_envs/load_latest_e3sm_unified.sh

# python /global/homes/w/whannah/e3sm_diags/run_e3sm_diags.py --case=$CASE 

# CASE=E3SM.PGVAL.ne30pg2_r05_oECv3.F2010SC5-CMIP6.master-cbe53b
# REF_CASE=E3SM.PGVAL.ne30_r05_oECv3.F2010SC5-CMIP6.master-cbe53b

# CASE=RGMA_LR_vs_Obs
# CASE=RGMA_MMF_vs_LR
CASE=RGMA_MMF_vs_Obs
# CASE=RGMA_HR_vs_LR
# CASE=RGMA_HR_vs_Obs

python $HOME/e3sm_diags/run_e3sm_diags.py --case=$CASE 

### update permissions
chmod -R 755 /global/cfs/cdirs/e3sm/www/whannah/
